var increasespeed ;
var Bullet = cc.Sprite.extend({
    ctor: function( x, y ) {
        this._super();
        this.initWithFile( 'res/images/zombieright2.png' );
        increasespeed =2;
    },
     randomPosition: function() {
     	var getlocation = Math.ceil(Math.random()*3);
     	if(getlocation == 1){
     		this.setPosition( new cc.Point(900,100) );
            increasespeed += 0.5 ;
     	}
         if(getlocation == 2){
     		this.setPosition( new cc.Point(900,220) );
            increasespeed += 0.5 ;
     	}
     	if(getlocation == 3){
     		this.setPosition( new cc.Point(900,340) );
            increasespeed += 0.5 ;
     	}
    },
    
       closeTo: function( obj ) {
	var myPos = this.getPosition();
	var oPos = obj.getPosition();

  	return ( ( Math.abs( myPos.x - oPos.x ) <= 40 ) &&
		 ( Math.abs( myPos.y - oPos.y ) <= 40 ) );
    },


    update: function( dt ) {
        this.setPositionX( this.getPositionX() - increasespeed );
        if(this.getPositionX()<-20){
            this.randomPosition();
            
        }
        if(increasespeed == 17)
            increasespeed = 17;
    }
 
});


var Bullet_2 = cc.Sprite.extend({
    ctor: function( x, y ) {
        this._super();
       this.initWithFile( 'res/images/zombieright2.png' );
 
    },
     randomPosition: function() {
        var getlocation = Math.ceil(Math.random()*3);
        if(getlocation == 1){
            this.setPosition( new cc.Point(1700,100) );
        }
         if(getlocation == 2){
            this.setPosition( new cc.Point(1700,220) );
        }
        if(getlocation == 3){
            this.setPosition( new cc.Point(1700,340) );
        }
    },

     
       closeTo: function( obj ) {
    var myPos = this.getPosition();
    var oPos = obj.getPosition();

    return ( ( Math.abs( myPos.x - oPos.x ) <= 40 ) &&
         ( Math.abs( myPos.y - oPos.y ) <= 40 ) );
    },


    update: function( dt ) {
        this.setPositionX( this.getPositionX() - 4);
         if(this.getPositionX()<-20){
            this.randomPosition();
        }
    }
 
});



var Bullet_3 = cc.Sprite.extend({
    ctor: function( x, y ) {
        this._super();
        this.initWithFile( 'res/images/zombieright2.png' );
 
    },
     randomPosition: function() {
        var getlocation = Math.ceil(Math.random()*3);
        if(getlocation == 1){
            this.setPosition( new cc.Point(10000,100) );
        }
         if(getlocation == 2){
            this.setPosition( new cc.Point(10000,220) );
        }
        if(getlocation == 3){
            this.setPosition( new cc.Point(10000,340) );
        }
    },

     
       closeTo: function( obj ) {
    var myPos = this.getPosition();
    var oPos = obj.getPosition();

    return ( ( Math.abs( myPos.x - oPos.x ) <= 40 ) &&
         ( Math.abs( myPos.y - oPos.y ) <= 40 ) );
    },


    update: function( dt ) {
        this.setPositionX( this.getPositionX() - 15);
         if(this.getPositionX()<-20){
            this.randomPosition();
        }
    }
 
});


var Bullet_4 = cc.Sprite.extend({
    ctor: function( x, y ) {
        this._super();
        this.initWithFile( 'res/images/zombieright2.png' );
 
    },
     randomPosition: function() {
        var getlocation = Math.ceil(Math.random()*3);
        if(getlocation == 1){
            this.setPosition( new cc.Point(20000,100) );
        }
         if(getlocation == 2){
            this.setPosition( new cc.Point(20000,220) );
        }
        if(getlocation == 3){
            this.setPosition( new cc.Point(20000,340) );
        }
    },

     
       closeTo: function( obj ) {
    var myPos = this.getPosition();
    var oPos = obj.getPosition();

    return ( ( Math.abs( myPos.x - oPos.x ) <= 40 ) &&
         ( Math.abs( myPos.y - oPos.y ) <= 40 ) );
    },


    update: function( dt ) {
        this.setPositionX( this.getPositionX() - 20);
         if(this.getPositionX()<-20){
            this.randomPosition();
        }
    }
 
});


var Bullet_5 = cc.Sprite.extend({
    ctor: function( x, y ) {
        this._super();
        this.initWithFile( 'res/images/zombieright2.png' );
 
    },
     randomPosition: function() {
        var getlocation = Math.ceil(Math.random()*3);
        if(getlocation == 1){
            this.setPosition( new cc.Point(20000,100) );
        }
         if(getlocation == 2){
            this.setPosition( new cc.Point(20000,220) );
        }
        if(getlocation == 3){
            this.setPosition( new cc.Point(20000,340) );
        }
    },

     
       closeTo: function( obj ) {
    var myPos = this.getPosition();
    var oPos = obj.getPosition();

    return ( ( Math.abs( myPos.x - oPos.x ) <= 40 ) &&
         ( Math.abs( myPos.y - oPos.y ) <= 40 ) );
    },


    update: function( dt ) {
        this.setPositionX( this.getPositionX() - 20);
         if(this.getPositionX()<-20){
            this.randomPosition();
        }
    }
 
});

var zomdrspeed;
var Zombiedrop = cc.Sprite.extend({
    ctor: function( x, y ) {
        this._super();
        this.initWithFile( 'res/images/zombiedrop.png' );
        zomdrspeed = 3;
 
    },
     
       closeTo: function( obj ) {
    var myPos = this.getPosition();
    var oPos = obj.getPosition();

    return ( ( Math.abs( myPos.x - oPos.x ) <= 40 ) &&
         ( Math.abs( myPos.y - oPos.y ) <= 40 ) );
    },


    update: function( dt ) {
        this.setPositionY( this.getPositionY() - zomdrspeed);
            if(this.getPositionY()< 200)
                zomdrspeed = 0;
    }
 
});

var Zombiehand = cc.Sprite.extend({
    ctor: function( x, y ) {
        this._super();
        this.initWithFile( 'res/images/zombiehand.png' );
 
    },
     randomPosition: function() {
        var getlocation = Math.ceil(Math.random()*6);
        if(getlocation == 1){
            this.setPosition( new cc.Point(200,120) );
        }
         if(getlocation == 2){
            this.setPosition( new cc.Point(-10,200) );
        }
        if(getlocation == 3){
            this.setPosition( new cc.Point(300,300) );
        }
        if(getlocation == 4){
            this.setPosition( new cc.Point(-10,400) );
        }
        if(getlocation == 5){
            this.setPosition( new cc.Point(100,500) );
        }
        if(getlocation == 6){
            this.setPosition( new cc.Point(-10,600) );
        }
        
    },

     


    update: function( dt ) {
        this.setPositionX( this.getPositionX() + 10);
         if(this.getPositionX()>900){
            this.randomPosition();
        }
    }
 
});
var speedzombieup;
var Zombieup = cc.Sprite.extend({
    ctor: function( x, y ) {
        this._super();
        this.initWithFile( 'res/images/zombieup.png' );
        speedzombieup = 0;
 
    },
     randomPosition: function() {
        var getlocation = Math.ceil(Math.random()*3);
        if(getlocation == 1){
            this.setPosition( new cc.Point(60,700) );
           
        }
        
    },

     
       closeTo: function( obj ) {
    var myPos = this.getPosition();
    var oPos = obj.getPosition();

    return ( ( Math.abs( myPos.x - oPos.x ) <= 50 ) &&
         ( Math.abs( myPos.y - oPos.y ) <= 50 ) );
    },


    update: function( dt ) {
        this.setPositionY( this.getPositionY() - speedzombieup);
         if(this.getPositionY()<-20){
            this.randomPosition();
            speedzombieup = 0;
        }
    }
 
});