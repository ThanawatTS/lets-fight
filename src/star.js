var axisx;
var Star = cc.Sprite.extend({
	
	ctor: function(x,y){
		this._super();
        this.initWithFile( 'res/images/star.png' );
    
        
	},

	  randomPosition: function() {
     	var getlocation = Math.ceil(Math.random()*3);
     	if(getlocation == 1){
     		this.setPosition( new cc.Point(900,60) );
     	}
         if(getlocation == 2){
     		this.setPosition( new cc.Point(900,180) );
     	}
     	if(getlocation == 3){
     		this.setPosition( new cc.Point(900,300) );
     	}
    },
    
    disappear: function(){
        this.setPosition( new cc.Point(-50,60) );
    },

    closeTo: function( obj ) {
    var myPos = this.getPosition();
    var oPos = obj.getPosition();

    return ( ( Math.abs( myPos.x - oPos.x ) <= 40 ) &&
         ( Math.abs( myPos.y - oPos.y ) <= 40 ) );
    },

    update: function(){
    
    	this.setPositionX( this.getPositionX() - 3 );

    }
   

});