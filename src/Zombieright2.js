var increasespeed ;
var zombieright2 = cc.Sprite.extend({
    ctor: function( x, y ) {
        this._super();
        this.initWithFile( 'res/images/zombieright2.png' );
        increasespeed =2;
    },
     randomPosition: function() {
     	var getlocation = Math.ceil(Math.random()*3);
     	if(getlocation == 1){
     		this.setPosition( new cc.Point(900,100) );
     	}
         if(getlocation == 2){
     		this.setPosition( new cc.Point(900,220) );
     	}
     	if(getlocation == 3){
     		this.setPosition( new cc.Point(900,340) );
     	}
    },
    
       closeTo: function( obj ) {
	var myPos = this.getPosition();
	var oPos = obj.getPosition();

  	return ( ( Math.abs( myPos.x - oPos.x ) <= 40 ) &&
		 ( Math.abs( myPos.y - oPos.y ) <= 40 ) );
    },


    update: function( dt ) {
        this.setPositionX( this.getPositionX() - increasespeed );
        if(this.getPositionX()<-10){
            this.randomPosition();
            increasespeed +=1;
        }
    }
 
});


var zombieright2_2 = cc.Sprite.extend({
    ctor: function( x, y ) {
        this._super();
       this.initWithFile( 'res/images/zombieright2.png' );
 
    },
     randomPosition: function() {
        var getlocation = Math.ceil(Math.random()*3);
        if(getlocation == 1){
            this.setPosition( new cc.Point(1700,100) );
        }
         if(getlocation == 2){
            this.setPosition( new cc.Point(1700,220) );
        }
        if(getlocation == 3){
            this.setPosition( new cc.Point(1700,340) );
        }
    },

     
       closeTo: function( obj ) {
    var myPos = this.getPosition();
    var oPos = obj.getPosition();

    return ( ( Math.abs( myPos.x - oPos.x ) <= 40 ) &&
         ( Math.abs( myPos.y - oPos.y ) <= 40 ) );
    },


    update: function( dt ) {
        this.setPositionX( this.getPositionX() - 4);
         if(this.getPositionX()<-10){
            this.randomPosition();
        }
    }
 
});



var zombieright2_3 = cc.Sprite.extend({
    ctor: function( x, y ) {
        this._super();
        this.initWithFile( 'res/images/zombieright2.png' );
 
    },
     randomPosition: function() {
        var getlocation = Math.ceil(Math.random()*3);
        if(getlocation == 1){
            this.setPosition( new cc.Point(10000,100) );
        }
         if(getlocation == 2){
            this.setPosition( new cc.Point(10000,220) );
        }
        if(getlocation == 3){
            this.setPosition( new cc.Point(10000,340) );
        }
    },

     
       closeTo: function( obj ) {
    var myPos = this.getPosition();
    var oPos = obj.getPosition();

    return ( ( Math.abs( myPos.x - oPos.x ) <= 40 ) &&
         ( Math.abs( myPos.y - oPos.y ) <= 40 ) );
    },


    update: function( dt ) {
        this.setPositionX( this.getPositionX() - 15);
         if(this.getPositionX()<-10){
            this.randomPosition();
        }
    }
 
});


var zombieright2_4 = cc.Sprite.extend({
    ctor: function( x, y ) {
        this._super();
        this.initWithFile( 'res/images/zombieright2.png' );
 
    },
     randomPosition: function() {
        var getlocation = Math.ceil(Math.random()*3);
        if(getlocation == 1){
            this.setPosition( new cc.Point(20000,100) );
        }
         if(getlocation == 2){
            this.setPosition( new cc.Point(20000,220) );
        }
        if(getlocation == 3){
            this.setPosition( new cc.Point(20000,340) );
        }
    },

     
       closeTo: function( obj ) {
    var myPos = this.getPosition();
    var oPos = obj.getPosition();

    return ( ( Math.abs( myPos.x - oPos.x ) <= 40 ) &&
         ( Math.abs( myPos.y - oPos.y ) <= 40 ) );
    },


    update: function( dt ) {
        this.setPositionX( this.getPositionX() - 20);
         if(this.getPositionX()<-10){
            this.randomPosition();
        }
    }
 
});


var zombieright2_5 = cc.Sprite.extend({
    ctor: function( x, y ) {
        this._super();
        this.initWithFile( 'res/images/zombieright2.png' );
 
    },
     randomPosition: function() {
        var getlocation = Math.ceil(Math.random()*3);
        if(getlocation == 1){
            this.setPosition( new cc.Point(20000,100) );
        }
         if(getlocation == 2){
            this.setPosition( new cc.Point(20000,220) );
        }
        if(getlocation == 3){
            this.setPosition( new cc.Point(20000,340) );
        }
    },

     
       closeTo: function( obj ) {
    var myPos = this.getPosition();
    var oPos = obj.getPosition();

    return ( ( Math.abs( myPos.x - oPos.x ) <= 40 ) &&
         ( Math.abs( myPos.y - oPos.y ) <= 40 ) );
    },


    update: function( dt ) {
        this.setPositionX( this.getPositionX() - 20);
         if(this.getPositionX()<-10){
            this.randomPosition();
        }
    }
 
});