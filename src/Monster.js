var speedmonster;
var harder;
var Monster = cc.Sprite.extend({
    ctor: function() {
    	speedmonster = 5;
        harder = 0;
        this._super();
        this.initWithFile( 'res/images/monster.png' );
        this.direction = Player.Move.UP;
    },

    closeTo: function( obj ) {
	var myPos = this.getPosition();
	var oPos = obj.getPosition();

  	return ( ( Math.abs( myPos.x - oPos.x ) <= 40 ) &&
		 ( Math.abs( myPos.y - oPos.y ) <= 40 ) );
    },

    hitmonster: function(){
    	speedmonster = -7;
    },


    update: function( dt ) {
        this.setPositionX( this.getPositionX() - speedmonster);
        if(this.getPositionX() == 680){
           speedmonster = 0.5 + harder;
        }
        if(this.getPositionX() >= 1800){
            this.setPosition(cc.p(1800,220));
             speedmonster = 5 ;
             harder += 0.15;
        }
           
        if(this.getPositionX() <= 60)
            speedmonster = 0 ;
    }

});